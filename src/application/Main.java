package application;
	
import com.sun.javafx.scene.control.skin.Utils;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
//CheckBox
/*Nesta atividade vamos uar CheckBoxs para selecionar v�rias op��es
 * Passo1 - criar 4 checkbox definir uma delas como selecionada
 * Passo2 - criar um bot�o para tratar da escola
 * 
 * Nota: Vamos precisar da alertbox de Utils
 * */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			
			//Cria 4 checkbox diferentes
			CheckBox cBox1 = new CheckBox("Op��o 1");
			CheckBox cBox2 = new CheckBox("Op��o 2");
			CheckBox cBox3 = new CheckBox("Op��o 3");
			CheckBox cBox4 = new CheckBox("Op��o 4");
			
			cBox3.setSelected(true);				//Ativa uma checkbox
			
			Button btnCheckSelectedBox = new Button("Compra");
			
			btnCheckSelectedBox.setOnAction(e ->  {
				if(cBox1.isSelected() ||	cBox2.isSelected()||
				   cBox3.isSelected() ||	cBox4.isSelected())
				{
					StringBuilder str = new StringBuilder();
					if(cBox1.isSelected()){str.append("CheckBox 1\n");}
					if(cBox2.isSelected()){str.append("CheckBox 2\n");}
					if(cBox3.isSelected()){str.append("CheckBox 3\n");}
					if(cBox4.isSelected()){str.append("CheckBox 4\n");}
					
					application.Utils.alertBox("Aten��o", "Foram Selecionadas:\n" +str);
				}
				else {
					application.Utils.alertBox("Aten��o", "N�o foi nada selecionado");
				}
			});
			
			VBox layoutCheckBox = new VBox(10);
			layoutCheckBox.setPadding(new Insets(20,20,20,20));
			layoutCheckBox.getChildren().addAll(cBox1,cBox2,cBox3,cBox4,btnCheckSelectedBox);
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layoutCheckBox,300,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
